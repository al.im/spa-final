package com.example.demo.controller;

import com.example.demo.model.Auth;
import com.example.demo.model.AuthDto;
import com.example.demo.model.Users;
import com.example.demo.repository.UsersRepository;
import com.example.demo.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/auth")
@AllArgsConstructor
public class AuthController {
    private final AuthService authService;
    private final UsersRepository usersRepository;

    @PostMapping
    public ResponseEntity<?> authentication(@RequestBody Auth auth, @RequestParam(name = "username") String username) {
        Users user = new Users(username);
        return ResponseEntity.ok("User created");
    }

    @GetMapping
    public ResponseEntity<?> authorization(@RequestBody AuthDto authDto) {
        try {
            return ResponseEntity.ok(authService.authorize(authDto));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

}
