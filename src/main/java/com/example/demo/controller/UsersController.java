package com.example.demo.controller;

import com.example.demo.model.Users;
import com.example.demo.repository.UsersRepository;
import com.example.demo.dto.User;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/v1/users")
@AllArgsConstructor
public class UsersController {
    private final UsersRepository usersRepository;


    @PostMapping("user")
    public User login(@RequestParam("name") String username, @RequestParam("password") String password) {

        String token = getJWTToken(username);
        User user = new User();
        user.setUser(username);
        user.setToken(token);
        return user;

    }

    private String getJWTToken(String username) {
        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("softtekJWT")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }



    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(usersRepository.findAll());
    }

    @PostMapping
    public ResponseEntity<?> saveUser(@RequestBody Users user) {
        return ResponseEntity.ok(usersRepository.save(user));
    }

    @PutMapping
    public ResponseEntity<?> updateUser(@RequestBody Users user) {
        return ResponseEntity.ok(usersRepository.save(user));
    }

    @DeleteMapping
    public ResponseEntity<?> deleteUser(@RequestParam(name = "id") Long id) {
        try {
            usersRepository.deleteById(id);
        } catch (Exception e) {
            return ResponseEntity.ok("user not found with id " + id);
        }
        return ResponseEntity.ok("user deleted");
    }


}
