package com.example.demo.controller;

import com.example.demo.model.Chat;
import com.example.demo.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/chats")
@AllArgsConstructor
public class ChatController {
    private final ChatService chatService;


    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(chatService.findAll());
    }

    @PostMapping
    public ResponseEntity<?> saveChat(@RequestBody Chat message) {
        return ResponseEntity.ok(chatService.save(message));
    }


    @PutMapping
    public ResponseEntity<?> updateChat(@RequestBody Chat message) {
        return ResponseEntity.ok(chatService.save(message));
    }

    @DeleteMapping
    public ResponseEntity<?> deleteChat(@RequestParam(name = "id") Long id) {
        try {
            chatService.deleteById(id);
        } catch (Exception e) {
            return ResponseEntity.ok("chat not found with id " + id);
        }
        return ResponseEntity.ok("chat deleted");
    }

}