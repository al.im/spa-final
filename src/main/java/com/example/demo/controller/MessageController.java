package com.example.demo.controller;

import com.example.demo.model.Auth;
import com.example.demo.model.Message;
import com.example.demo.service.AuthService;
import com.example.demo.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/messages")
@AllArgsConstructor
public class MessageController {
    private final MessageService messageService;
    private final AuthService authService;

    @PostMapping
    public ResponseEntity<?> saveMessage(@RequestBody Message message, @RequestHeader(name = "token") String token) {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("error");
        }
        try {
            messageService.addMessage(message);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("message added");
    }

    @PutMapping
    public ResponseEntity<?> updateMessage(@RequestBody Message message,@RequestHeader(name = "token") String token) {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("error");
        }
        try {
            messageService.updateMessage(message);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("message updated");
    }

    @DeleteMapping
    public ResponseEntity<?> deleteMessage(@RequestParam(name = "id") Long id,@RequestHeader(name = "token") String token) {
        Auth auth = authService.getByToken(token);
        if (auth == null) {
            return ResponseEntity.badRequest().body("error");
        }
        try {
            messageService.deleteById(id);
        } catch (Exception e) {
            return ResponseEntity.ok("message not found with id " + id);
        }
        return ResponseEntity.ok("message deleted");
    }

}
