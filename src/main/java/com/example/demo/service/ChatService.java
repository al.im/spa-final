package com.example.demo.service;

import com.example.demo.model.Chat;
import com.example.demo.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@AllArgsConstructor
public class ChatService {
    private final ChatRepository chatRepository;

    public List<Chat> findAll() {
        return chatRepository.findAll();
    }

    public Chat save(Chat message) {
        return chatRepository.save(message);
    }

    public void deleteById(Long id) {
        chatRepository.deleteById(id);
    }
}
