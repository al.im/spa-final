package com.example.demo.service;


import com.example.demo.model.Message;
import com.example.demo.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    public Message addMessage(Message message) throws Exception {
        if (message == null) throw new Exception("message is null");
        if (message.getId() != null) throw new Exception("message has id");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");

        return messageRepository.save(message);
    }

    public Message updateMessage(Message message) throws Exception {
        if (message == null) throw new Exception("message is null");
        if (message.getId() == null) throw new Exception("message id is null");
        if (message.getText() == null || message.getText().isBlank()) throw new Exception("message is blank");


        Optional<Message> messageDB = messageRepository.findById(message.getId());
        if (messageDB.isEmpty()) throw new Exception("message not found");

        String text = message.getText();
        message = messageDB.get();
        Long date = new Date().getTime();
        message.setText(text);

        return messageRepository.save(message);
    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }

}
