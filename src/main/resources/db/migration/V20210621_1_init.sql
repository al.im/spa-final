drop table if exists users;
create table users(
    id serial,
    name varchar(255)
);

drop table if exists chat;
create table chat(
    id serial,
    name varchar(255)
);

drop table if exists messages;
create table messages(
    id serial,
    user_id bigint,
    chat_id bigint,
    text varchar(255)
);

drop table if exists auth;
create table auth(
    id bigint,
    login varchar(255),
    password varchar(255),
    user_id bigint,
    token varchar(255)
);
